<?php

namespace App\Http\Controllers\baongoc;

use App\Http\Controllers\Controller;
use App\Models\ShopBaoNgoc;
use Illuminate\Http\Request;

class ShopBaoNgocController extends Controller
{
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		return view('baongoc.shop.index');
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		return view('baongoc.shop.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  \App\Models\ShopBaoNgoc  $shopBaoNgoc
	 * @return \Illuminate\Http\Response
	 */
	public function show(ShopBaoNgoc $shopBaoNgoc)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  \App\Models\ShopBaoNgoc  $shopBaoNgoc
	 * @return \Illuminate\Http\Response
	 */
	public function edit(ShopBaoNgoc $shopBaoNgoc)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \App\Models\ShopBaoNgoc  $shopBaoNgoc
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, ShopBaoNgoc $shopBaoNgoc)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  \App\Models\ShopBaoNgoc  $shopBaoNgoc
	 * @return \Illuminate\Http\Response
	 */
	public function destroy(ShopBaoNgoc $shopBaoNgoc)
	{
		//
	}
}
