<?php

namespace App\Http\Controllers\baongoc;

use App\Http\Controllers\Controller;
use App\Models\ApiBaoNgoc;
use Illuminate\Http\Request;

class ApiBaoNgocController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('baongoc.api.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ApiBaoNgoc  $apiBaoNgoc
     * @return \Illuminate\Http\Response
     */
    public function show(ApiBaoNgoc $apiBaoNgoc)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\ApiBaoNgoc  $apiBaoNgoc
     * @return \Illuminate\Http\Response
     */
    public function edit(ApiBaoNgoc $apiBaoNgoc)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\ApiBaoNgoc  $apiBaoNgoc
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ApiBaoNgoc $apiBaoNgoc)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ApiBaoNgoc  $apiBaoNgoc
     * @return \Illuminate\Http\Response
     */
    public function destroy(ApiBaoNgoc $apiBaoNgoc)
    {
        //
    }
}
