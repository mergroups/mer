<div class="vertical-menu">

    <div data-simplebar class="h-100">

        <!--- Sidemenu -->
        <div id="sidebar-menu">
            <!-- Left Menu Start -->
            <ul class="metismenu list-unstyled" id="side-menu">
                <li class="menu-title" key="t-menu">MER</li>
                <li>
                    <a href="#" class="waves-effect">
                        <i class="bx bx-home-circle"></i><span class="badge rounded-pill bg-info float-end">01</span>
                        <span key="t-dashboards">Dashboards</span>
                    </a>
                    <ul class="sub-menu" aria-expanded="false">
                        <li><a href="#" key="t-default">Products</a></li>
                        <li><a href="#" key="t-default">Category</a></li>
                        <li><a href="#" key="t-default">SubCategory</a></li>
                        <li><a href="#" key="t-default">Shipping</a></li>
                        <li><a href="#" key="t-default">User</a></li>
                        <li><a href="#" key="t-default">Customer</a></li>
                        <li><a href="#" key="t-default">Invoice</a></li>
                        <li><a href="#" key="t-default">Blog</a></li>
                        <li><a href="#" key="t-default">API</a></li>
                    </ul>
                </li>

                <li class="menu-title" key="t-menu">BaoNgoc</li>
                <li>
                    <a href="#" class="waves-effect">
                        <i class="bx bx-home-circle"></i><span class="badge rounded-pill bg-info float-end">04</span>
                        <span key="t-dashboards">Dashboards</span>
                    </a>
                    <ul class="sub-menu" aria-expanded="false">
                        <li><a href="#" key="t-default">Products</a></li>
                        <li><a href="#" key="t-default">Category</a></li>
                        <li><a href="#" key="t-default">SubCategory</a></li>
                        <li><a href="#" key="t-default">Shipping</a></li>
                        <li><a href="#" key="t-default">User</a></li>
                        <li><a href="#" key="t-default">Customer</a></li>
                        <li><a href="#" key="t-default">Invoice</a></li>
                        <li><a href="#" key="t-default">Blog</a></li>
                        <li><a href="#" key="t-default">API</a></li>
                    </ul>
                </li>


            </ul>
        </div>
        <!-- Sidebar -->
    </div>
</div>
