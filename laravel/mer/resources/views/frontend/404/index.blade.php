@extends('frontend.layouts.app')

@section('title')
	<title>{{ __('404') }}</title>
@endsection

@section('content')
	<!-- header start -->
	@include('frontend.layouts.header')
	<!--  header end -->

	<!-- breadcrumb start -->
	<section class="order-food-section not-found  pt-0">
		<img src="{{ asset('frontend/assets/images/restaurant/background/1.jpg') }}" class="bg-img img-fluid blur-up lazyload" alt="">
		<div class="order-food">
			<div class="container">
				<div class="row">
					<div class="col-lg-8 offset-lg-2">
						<div class="book-table section-b-space p-0 single-table">
							<h3>The food you love, delivered with care</h3>
							<div class="table-form">
								<form>
									<div class="row w-100">
										<div class="form-group col-md-4">
											<input type="text" placeholder="enter your location" class="form-control">
										</div>
										<div class="form-group col-md-5">
											<input type="text" placeholder="what are you craving?" class="form-control">
										</div>
										<div class="search col-md-3">
											<a href="#" class="btn btn-rounded color1">find food</a>
										</div>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- breadcrumb end -->


	<!-- section start -->
	<section class="small-section success-section">
		<div class="container">
			<div class="row success-detail mt-0">
				<div class="col">
					<img src="{{ asset('frontend/assets/images/restaurant/serving-dish.png') }}" class="img-fluid blur-up lazyload" alt="">
					<h2>oops ! no food or restaurant found</h2>
					<p>We can’t find anything related to your search. Try a different search.</p>
					<a href="{{ route('home.index') }}" class="btn btn-rounded btn-sm color1">book now</a>
				</div>
			</div>
		</div>
	</section>
	<!-- section End -->


	<!-- footer start -->
	@include('frontend.layouts.footer')
	<!-- footer end -->

	<!-- tap to top -->
	@include('frontend.layouts.top')
	<!-- tap to top end -->

	<!-- setting start -->
	@include('frontend.layouts.setting')
	<!-- setting end -->

@endsection
