<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="rica">
	<meta name="keywords" content="rica">
	<meta name="author" content="rica">
	@yield('meta')

	<!-- CSRF Token -->
	<meta name="csrf-token" content="{{ csrf_token() }}">
	@yield('title')

	<link rel="icon" href="{{ asset('frontend/assets/images/favicon.png')}}" type="image/x-icon" />
	<link href="{{asset('//fonts.googleapis.com/css?family=Nunito:300,300i,400,400i,600,600i,700,700i,800,800i,900,900i&display=swap')}}" rel="stylesheet">
	<link href="{{asset('//fonts.googleapis.com/css?family=Work+Sans:400,500,600,700,800,900&display=swap')}}" rel="stylesheet">
	<link href="{{asset('//fonts.googleapis.com/css?family=Vampiro+One&display=swap')}}" rel="stylesheet">
	<link href="{{asset('//fonts.googleapis.com/css?family=Pacifico&display=swap')}}" rel="stylesheet">
	<link href="{{asset('//fonts.googleapis.com/css?family=Bangers&display=swap')}}" rel="stylesheet">

	<link rel="stylesheet" type="text/css" href="{{ asset('frontend/assets/css/font-awesome.css')}}">
	<link rel="stylesheet" type="text/css" href="{{ asset("frontend/assets/css/animate.css")}}">
	<link rel="stylesheet" type="text/css" href="{{ asset('frontend/assets/css/price-range.css')}}">
	<link rel="stylesheet" type="text/css" href="{{ asset('frontend/assets/css/slick.css')}}">
	<link rel="stylesheet" type="text/css" href="{{ asset('frontend/assets/css/slick-theme.css')}}">
	<link rel="stylesheet" type="text/css" href="{{ asset('frontend/assets/css/bootstrap.css')}}">
	<link rel="stylesheet" type="text/css" href="{{ asset('frontend/assets/css/color1.css')}}">

	@yield('css')
</head>


<body>
	@yield('content')

	<script src="{{ asset('frontend/assets/js/jquery-3.5.1.min.js')}}"></script>

	<!-- filter js -->
	<script src="{{ asset('frontend/assets/js/filter.js')}}"></script>
	<script src="{{ asset('frontend/assets/js/isotope.min.js')}}"></script>

	<!-- jarallax effect js -->
	<script src="{{ asset('frontend/assets/js/jarallax-min-0.2.1.js')}}"></script>
	<script src="{{ asset('frontend/assets/js/custom-jarallax.js')}}"></script>

	<!-- price range js -->
	<script src="{{ asset('frontend/assets/js/price-range.js')}}"></script>

	<!-- slick js-->
	<script src="{{ asset('frontend/assets/js/slick.js')}}"></script>

	<!-- Bootstrap js-->
	<script src="{{ asset('frontend/assets/js/bootstrap.bundle.min.js')}}"></script>

	<!-- lazyload js-->
	<script src="{{ asset('frontend/assets/js/lazysizes.min.js')}}"></script>

	<!-- Theme js-->
	<script src="{{ asset('frontend/assets/js/script.js')}}"></script>
</body>

</html>
