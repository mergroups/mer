<header class="light_header">
	<div class="container">
		<div class="row">
			<div class="col">
				<div class="menu">
					<nav>
						<div class="main-navbar">
							<div id="mainnav">
								<div class="toggle-nav"><i class="fa fa-bars sidebar-bar"></i></div>
								<div class="menu-overlay"></div>
								<ul class="nav-menu">
									<li class="back-btn">
										<div class="mobile-back text-end">
											<span>{{ __('Back') }}</span>
											<i aria-hidden="true" class="fa fa-angle-right ps-2"></i>
										</div>
									</li>
									<li class="dropdown">
										<a href="#" class="nav-link menu-title">{{ __('Bao Ngoc') }}</a>
										<ul class="nav-submenu menu-content">
											<li>
												<a href="{{ route('category.index') }}" >{{ __('Product Category') }}</a>
											</li>
											<li>
												<a href="{{ route('blogs.index') }}" >{{ __('Blogs') }}</a>
											</li>
											<li>
												<a href="#" >{{ __('Product Detail') }}</a>
											</li>
											<li>
												<a href="#" >{{ __('SubCategory') }}</a>
											</li>
											<li>
												<a href="#" >{{ __('Account') }}</a>
											</li>
											<li>
												<a href="#" >{{ __('Cart') }}</a>
											</li>
                                            <li>
                                                <a href="#" >{{ __('Shipping') }}</a>
                                            </li>
										</ul>
									</li>
									<li class="dropdown">
										<a class="nav-link menu-title">
											@if ( Lang::locale() == 'en')
												{{ 'English' }}
											@elseif ( Lang::locale() == 'vi' )
												{{ 'VietNamese' }}
											@endif
										</a>
										<ul class="nav-submenu menu-content">
											<li><a href="{!! route('user.change-language', ['en']) !!}"  >English</a></li>
											<li><a href="{!! route('user.change-language', ['vi']) !!}"  >VietNamese</a></li>
										</ul>
									</li>



								</ul>
							</div>
						</div>
					</nav>




				</div>
			</div>
		</div>
	</div>
</header>
