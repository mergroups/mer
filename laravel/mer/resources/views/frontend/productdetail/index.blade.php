@extends('frontend.layouts.app')

@section('title')
    <title>{{ __('Category') }}</title>
@endsection

@section('content')

    <!-- header start -->
    @include('frontend.layouts.header')
    <!--  header end -->








    <!-- footer start -->
    @include('frontend.layouts.footer')
    <!-- footer end -->


    <!-- tap to top -->
    <div class="tap-top">
        <div>
            <i class="fas fa-angle-up"></i>
        </div>
    </div>
    <!-- tap to top end -->


    <!-- setting start -->
    @include('frontend.layouts.setting')
    <!-- setting end -->

    <!-- latest jquery-->

@endsection
