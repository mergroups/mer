@extends('frontend.layouts.app')

@section('title')
    <title>{{ __('Category') }}</title>
@endsection

@section('content')

    <!-- header start -->
    @include('frontend.layouts.header')
    <!--  header end -->

    <!-- breadcrumb start -->
    <section class="breadcrumb-section pt-0">
        <img src="{{ asset('frontend/assets/images/tour/inner-page/breadcrumb.jpg') }}" class="bg-img img-fluid blur-up lazyload" alt="">
        <div class="breadcrumb-content pt-0">
            <div>
                <h2>blog</h2>
                <nav aria-label="breadcrumb" class="theme-breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="http://themes.pixelstrap.com/rica/html/index.html">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">blog</li>
                    </ol>
                </nav>
            </div>
        </div>
        <div class="title-breadcrumb">Rica</div>
    </section>
    <!-- breadcrumb end -->


    <!-- blog section start -->
    <section class="section-b-space bg-white">
        <div class="container">
            <div class="row">
                <div class="col-lg-3">
                    <div class="sticky-cls-top">
                        <div class="blog-sidebar">
                            <div class="blog-wrapper">
                                <div class="search-bar">
                                    <input type="text" placeholder="Search here..">
                                    <i class="fas fa-search"></i>
                                </div>
                            </div>
                            <div class="blog-wrapper">
                                <div class="sidebar-title">
                                    <h5>categories</h5>
                                </div>
                                <div class="sidebar-content">
                                    <ul class="sidebar-list">
                                        <li class="">
                                            <a href="#">
                                                <i aria-hidden="true" class="fa fa-angle-right"></i>Lorem Ipsum Is
                                                Simple
                                            </a>
                                        </li>
                                        <li class="">
                                            <a href="#">
                                                <i aria-hidden="true" class="fa fa-angle-right"></i>Many Variations
                                            </a>
                                        </li>
                                        <li class="">
                                            <a href="#">
                                                <i aria-hidden="true" class="fa fa-angle-right"></i>random text
                                            </a>
                                        </li>
                                        <li class="">
                                            <a href="#">
                                                <i aria-hidden="true" class="fa fa-angle-right"></i>lorem ipsum.
                                            </a>
                                        </li>
                                        <li class="">
                                            <a href="#">
                                                <i aria-hidden="true" class="fa fa-angle-right m-r-15"></i>it's Random.
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="blog-wrapper">
                                <div class="sidebar-title">
                                    <h5>popular post</h5>
                                </div>
                                <div class="sidebar-content">
                                    <ul class="blog-post">
                                        <li>
                                            <div class="media">
                                                <img class="img-fluid blur-up lazyload"
                                                     src="{{ asset('frontend/assets/images/portfolio/6.jpg')}}"
                                                     alt="Generic placeholder image">
                                                <div class="media-body align-self-center">
                                                    <div>
                                                        <h6>25 Dec 2018</h6>
                                                        <p>100 hits</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="media">
                                                <img class="img-fluid blur-up lazyload"
                                                     src="{{ asset('frontend/assets/images/portfolio/7.jpg')}}"
                                                     alt="Generic placeholder image">
                                                <div class="media-body align-self-center">
                                                    <div>
                                                        <h6>25 Dec 2018</h6>
                                                        <p>540 hits</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="media">
                                                <img class="img-fluid blur-up lazyload"
                                                     src="{{ asset('frontend/assets/images/portfolio/8.jpg')}}"
                                                     alt="Generic placeholder image">
                                                <div class="media-body align-self-center">
                                                    <div>
                                                        <h6>25 Dec 2018</h6>
                                                        <p>250 hits</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="media">
                                                <img class="img-fluid blur-up lazyload"
                                                     src="{{ asset('frontend/assets/images/portfolio/2.jpg')}}"
                                                     alt="Generic placeholder image">
                                                <div class="media-body align-self-center">
                                                    <div>
                                                        <h6>25 Dec 2018</h6>
                                                        <p>30 hits</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="blog-wrapper">
                                <div class="sidebar-title">
                                    <h5>popular tags</h5>
                                </div>
                                <div class="sidebar-content">
                                    <ul class="tags">
                                        <li><a href="#">adventure</a></li>
                                        <li><a href="#">luxury</a></li>
                                        <li><a href="#">romantic</a></li>
                                        <li><a href="#">foodie</a></li>
                                        <li><a href="#">villas</a></li>
                                        <li><a href="#">cruise</a></li>
                                        <li><a href="#">shopping</a></li>
                                        <li><a href="#">beach</a></li>
                                        <li><a href="#">family</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-9">
                    <div class="blog_section blog-inner ratio_landscape">
                        <div class="row blog-list">
                            <div class="col-12">
                                <div class="blog-wrap">
                                    <div class="blog-image">
                                        <a href="#">
                                            <img src="{{ asset('frontend/assets/images/portfolio/4.jpg')}}"
                                                 class="img-fluid blur-up lazyload bg-img" alt="">
                                        </a>
                                    </div>
                                    <div class="blog-details">
                                        <div>
                                            <h6>
                                                <i class="fas fa-map-marker-alt"></i> phonics, newyork
                                                <i class="fas fa-clock ms-2"></i> 20 april, 2019
                                            </h6>
                                            <a href="#">
                                                <h5>Twice profit than before you. </h5>

                                            </a>
                                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting
                                                industry. Lorem Ipsum
                                                has been the industry's standard dummy text...</p>
                                            <h6 class="link"><a href="#">read more</a></h6>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="blog-wrap">
                                    <div class="blog-image order-md-1">
                                        <a href="#">
                                            <img src="{{ asset('frontend/assets/images/portfolio/2.jpg')}}"
                                                 class="img-fluid blur-up lazyload bg-img" alt="">
                                        </a>
                                    </div>
                                    <div class="blog-details text-md-right">
                                        <div>
                                            <h6>
                                                <i class="fas fa-map-marker-alt"></i> phonics, newyork
                                                <i class="fas fa-clock ms-2"></i> 20 april, 2019
                                            </h6>
                                            <a href="#">
                                                <h5>Twice profit than before you. </h5>

                                            </a>
                                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting
                                                industry. Lorem Ipsum
                                                has been the industry's standard dummy text...</p>
                                            <h6 class="link"><a href="#">read more</a></h6>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="blog-wrap">
                                    <div class="blog-image">
                                        <a href="#">
                                            <img src="{{ asset('frontend/assets/images/portfolio/10.jpg')}}"
                                                 class="img-fluid blur-up lazyload bg-img" alt="">
                                        </a>
                                    </div>
                                    <div class="blog-details">
                                        <div>
                                            <h6>
                                                <i class="fas fa-map-marker-alt"></i> phonics, newyork
                                                <i class="fas fa-clock ms-2"></i> 20 april, 2019
                                            </h6>
                                            <a href="#">
                                                <h5>Twice profit than before you. </h5>

                                            </a>
                                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting
                                                industry. Lorem Ipsum
                                                has been the industry's standard dummy text...</p>
                                            <h6 class="link"><a href="#">read more</a></h6>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="blog-wrap">
                                    <div class="blog-image order-md-1">
                                        <a href="#">
                                            <img src="{{ asset('frontend/assets/images/portfolio/11.jpg')}}"
                                                 class="img-fluid blur-up lazyload bg-img" alt="">
                                        </a>
                                    </div>
                                    <div class="blog-details text-md-right">
                                        <div>
                                            <h6>
                                                <i class="fas fa-map-marker-alt"></i> phonics, newyork
                                                <i class="fas fa-clock ms-2"></i> 20 april, 2019
                                            </h6>
                                            <a href="#">
                                                <h5>Twice profit than before you. </h5>

                                            </a>
                                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting
                                                industry. Lorem Ipsum
                                                has been the industry's standard dummy text...</p>
                                            <h6 class="link"><a href="#">read more</a></h6>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="blog-wrap">
                                    <div class="blog-image">
                                        <a href="#">
                                            <img src="{{ asset('frontend/assets/images/portfolio/12.jpg')}}"
                                                 class="img-fluid blur-up lazyload bg-img" alt="">
                                        </a>
                                    </div>
                                    <div class="blog-details">
                                        <div>
                                            <h6>
                                                <i class="fas fa-map-marker-alt"></i> phonics, newyork
                                                <i class="fas fa-clock ms-2"></i> 20 april, 2019
                                            </h6>
                                            <a href="#">
                                                <h5>Twice profit than before you. </h5>

                                            </a>
                                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting
                                                industry. Lorem Ipsum
                                                has been the industry's standard dummy text...</p>
                                            <h6 class="link"><a href="#">read more</a></h6>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <nav aria-label="Page navigation example" class="pagination-section mt-0">
                            <ul class="pagination">
                                <li class="page-item">
                                    <a class="page-link" href="#" aria-label="Previous">
                                        <span aria-hidden="true">&laquo;</span>
                                        <span class="sr-only">Previous</span>
                                    </a>
                                </li>
                                <li class="page-item active"><a class="page-link" href="#">1</a></li>
                                <li class="page-item"><a class="page-link" href="#">2</a></li>
                                <li class="page-item"><a class="page-link" href="#">3</a></li>
                                <li class="page-item">
                                    <a class="page-link" href="#" aria-label="Next">
                                        <span aria-hidden="true">&raquo;</span>
                                        <span class="sr-only">Next</span>
                                    </a>
                                </li>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- blog section end -->

    <!-- footer start -->
    @include('frontend.layouts.footer')
    <!-- footer end -->


    <!-- tap to top -->
    <div class="tap-top">
        <div>
            <i class="fas fa-angle-up"></i>
        </div>
    </div>
    <!-- tap to top end -->


    <!-- setting start -->
    @include('frontend.layouts.setting')
    <!-- setting end -->

    <!-- latest jquery-->

@endsection
