@extends('frontend.layouts.app')

@section('title')
	<title>{{ __('SubCategory') }}</title>
@endsection

@section('content')
	<!-- header start -->
	@include('frontend.layouts.header')
	<!--  header end -->








	<!-- footer start -->
	@include('frontend.layouts.footer')
	<!-- footer end -->

	<!-- tap to top -->
	@include('frontend.layouts.top')
	<!-- tap to top end -->

	<!-- setting start -->
	@include('frontend.layouts.setting')
	<!-- setting end -->

@endsection
