@extends('frontend.layouts.app')

@section('title')
	<title>{{ __('Category') }}</title>
@endsection

@section('content')

	<!-- header start -->
	@include('frontend.layouts.header')
	<!--  header end -->

	<!-- section start -->
	<section class="order-food-section  pt-0">
		<img src="{{ asset('frontend/assets/images/restaurant/background/1.jpg')}}"
			 class="bg-img img-fluid blur-up lazyload" alt="">
		<div class="order-food">
			<div class="container">
				<div class="row">
					<div class="col-lg-8 offset-lg-2">
						<div class="book-table section-b-space p-0 single-table">
							<h3>The food you love, delivered with care</h3>
							<div class="table-form">
								<form>
									<div class="row w-100">
										<div class="form-group col-md-4">
											<input type="text" placeholder="enter your location" class="form-control">
										</div>
										<div class="form-group col-md-5">
											<input type="text" placeholder="what are you craving?" class="form-control">
										</div>
										<div class="search col-md-3">
											<a href="#" class="btn btn-rounded color1">find food</a>
										</div>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- section end -->

	<!-- section start -->
	<section class="small-section bg-inner">
		<div class="container">
			<div class="row">
				<div class="col-12">
					<div class="filter-panel">
						<div class="left-filter">
							<div class="respon-filter-btn">
								<h6> filter <i class="fas fa-sort-down"></i></h6>
								<span class="according-menu"></span>
							</div>
							<div class="filters respon-filter-content filter-button-group">
								<ul>
									<li class="active" data-filter="*">All</li>
									<li data-filter=".popular">popular</li>
									<li data-filter=".latest">latest</li>
									<li data-filter=".trend">trend</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-3">
					<div class="left-sidebar">
						<div class="back-btn">
							back
						</div>
						<div class="search-bar">
							<input type="text" placeholder="Search here..">
							<i class="fas fa-search"></i>
						</div>
						<div class="middle-part collection-collapse-block open">
							<a href="javascript:void(0)" class="section-title collapse-block-title">
								<h5>latest filter</h5>
								<img src="{{ asset('frontend/assets/images/icon/adjust.png')}}"
									 class="img-fluid blur-up lazyload" alt="">
							</a>
							<div class="collection-collapse-block-content ">
								<div class="filter-block">
									<div class="collection-collapse-block open">
										<h6 class="collapse-block-title">popular</h6>
										<div class="collection-collapse-block-content">
											<div class="collection-brand-filter">
												<div class="form-check collection-filter-checkbox">
													<input type="checkbox" class="form-check-input" id="free-d">
													<label class="form-check-label" for="free-d">free
														delivery</label>
												</div>
												<div class="form-check collection-filter-checkbox">
													<input type="checkbox" class="form-check-input" id="time">
													<label class="form-check-label" for="time">reached in 20
														min</label>
												</div>
												<div class="form-check collection-filter-checkbox">
													<input type="checkbox" class="form-check-input" id="zara">
													<label class="form-check-label" for="zara">pure veg</label>
												</div>
												<div class="form-check collection-filter-checkbox">
													<input type="checkbox" class="form-check-input" id="vera-moda">
													<label class="form-check-label" for="vera-moda">non veg</label>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="filter-block">
									<div class="collection-collapse-block open">
										<h6 class="collapse-block-title">cuisines</h6>
										<div class="collection-collapse-block-content">
											<div class="collection-brand-filter">
												<div class="form-check collection-filter-checkbox">
													<input type="checkbox" class="form-check-input" id="wifi">
													<label class="form-check-label" for="wifi">Asian</label>
												</div>
												<div class="form-check collection-filter-checkbox">
													<input type="checkbox" class="form-check-input" id="spa">
													<label class="form-check-label" for="spa">Seafood</label>
												</div>
												<div class="form-check collection-filter-checkbox">
													<input type="checkbox" class="form-check-input" id="pet">
													<label class="form-check-label" for="pet">Italian</label>
												</div>
												<div class="form-check collection-filter-checkbox">
													<input type="checkbox" class="form-check-input" id="parking">
													<label class="form-check-label" for="parking">Pizza</label>
												</div>
												<div class="form-check collection-filter-checkbox">
													<input type="checkbox" class="form-check-input" id="swimming">
													<label class="form-check-label" for="swimming">Western</label>
												</div>
												<div class="form-check collection-filter-checkbox">
													<input type="checkbox" class="form-check-input" id="chinese">
													<label class="form-check-label" for="chinese">Chinese</label>
												</div>
												<div class="form-check collection-filter-checkbox">
													<input type="checkbox" class="form-check-input" id="restaurant">
													<label class="form-check-label" for="restaurant">Dessert</label>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="filter-block">
									<div class="collection-collapse-block open">
										<h6 class="collapse-block-title">star category</h6>
										<div class="collection-collapse-block-content">
											<div class="collection-brand-filter">
												<div class="form-check collection-filter-checkbox">
													<input type="checkbox" class="form-check-input" id="five">
													<label class="form-check-label rating" for="five">
														<i class="fas fa-star"></i>
														<i class="fas fa-star"></i>
														<i class="fas fa-star"></i>
														<i class="fas fa-star"></i>
														<i class="fas fa-star"></i>
														<span class="ms-1">(4025)</span>
													</label>
												</div>
												<div class="form-check collection-filter-checkbox">
													<input type="checkbox" class="form-check-input" id="four">
													<label class="form-check-label rating" for="four">
														<i class="fas fa-star"></i>
														<i class="fas fa-star"></i>
														<i class="fas fa-star"></i>
														<i class="fas fa-star"></i>
														<i class="far fa-star"></i>
														<span class="ms-1">(2012)</span>
													</label>
												</div>
												<div class="form-check collection-filter-checkbox">
													<input type="checkbox" class="form-check-input" id="three">
													<label class="form-check-label rating" for="three">
														<i class="fas fa-star"></i>
														<i class="fas fa-star"></i>
														<i class="fas fa-star"></i>
														<i class="far fa-star"></i>
														<i class="far fa-star"></i>
														<span class="ms-1">(25)</span>
													</label>
												</div>
												<div class="form-check collection-filter-checkbox">
													<input type="checkbox" class="form-check-input" id="two">
													<label class="form-check-label rating" for="two">
														<i class="fas fa-star"></i>
														<i class="fas fa-star"></i>
														<i class="far fa-star"></i>
														<i class="far fa-star"></i>
														<i class="far fa-star"></i>
														<span class="ms-1">(1)</span>
													</label>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="filter-block">
									<div class="collection-collapse-block open">
										<h6 class="collapse-block-title">cost for two</h6>
										<div class="collection-collapse-block-content">
											<div class="wrapper">
												<div class="range-slider">
													<input type="text" class="js-range-slider" value=""/>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="filter-block">
									<div class="collection-collapse-block open">
										<h6 class="collapse-block-title">delivery time</h6>
										<div class="collection-collapse-block-content">
											<div class="collection-brand-filter">
												<div class="form-check collection-filter-checkbox">
													<input type="checkbox" class="form-check-input" id="suomi">
													<label class="form-check-label" for="suomi">upto 20
														minutes</label>
												</div>
												<div class="form-check collection-filter-checkbox">
													<input type="checkbox" class="form-check-input" id="english">
													<label class="form-check-label" for="english">upto 30
														minutes</label>
												</div>
												<div class="form-check collection-filter-checkbox">
													<input type="checkbox" class="form-check-input" id="sign">
													<label class="form-check-label" for="sign">upto 45
														minutes</label>
												</div>
												<div class="form-check collection-filter-checkbox">
													<input type="checkbox" class="form-check-input" id="italiano">
													<label class="form-check-label" for="italiano">upto 60
														minutes</label>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="bottom-info">
							<h5><span>i</span> need help</h5>
							<h4>856 - 215 - 211</h4>
							<h6>Monday to Friday 9.00am - 7.30pm</h6>
						</div>
					</div>
				</div>
				<div class="col-lg-9 ratio3_2">
					<a href="javascript:void(0)" class="mobile-filter border-top-0">
						<h5>latest filter</h5>
						<img src="{{ asset('frontend/assets/images/icon/adjust.png')}}"
							 class="img-fluid blur-up lazyload" alt="">
					</a>
					<div class="product-wrapper-grid special-section grid-box">
						<div class="row content grid">
							<div class="col-lg-4 col-sm-6 popular grid-item wow fadeInUp">
								<div class="special-box p-0 slider-sec">
									<div class="special-img">
										<div class="slide-1">
											<div>
												<img src="{{ asset('frontend/assets/images/restaurant/dishes/7.jpg')}}"
													 class="img-fluid blur-up lazyload bg-img" alt="">
											</div>
											<div>
												<img src="{{ asset('frontend/assets/images/restaurant/dishes/8.jpg')}}"
													 class="img-fluid blur-up lazyload bg-img" alt="">
											</div>
										</div>
									</div>
									<div class="special-content restaurant-detail">
										<a href="restaurant-single-1.html">
											<h5>italian restro
												<span class="positive">4.5 <i class="fas fa-star"></i></span>
											</h5>
										</a>
										<ul>
											<li>fast food, cafe, italian</li>
											<li>11.30am - 11.30pm (mon-sun)</li>
											<li>cost $25 for two</li>
										</ul>
									</div>
									<div class="label-offer">Recommended</div>
								</div>
							</div>
							<div class="col-lg-4 col-sm-6 latest grid-item wow fadeInUp">
								<div class="special-box p-0 slider-sec">
									<div class="special-img">
										<div class="slide-1">
											<div>
												<img src="{{ asset('frontend/assets/images/restaurant/dishes/8.jpg')}}"
													 class="img-fluid blur-up lazyload bg-img" alt="">
											</div>
											<div>
												<img src="{{ asset('frontend/assets/images/restaurant/dishes/9.jpg')}}"
													 class="img-fluid blur-up lazyload bg-img" alt="">
											</div>
										</div>
									</div>
									<div class="special-content restaurant-detail">
										<a href="restaurant-single-1.html">
											<h5>italian restro
												<span class="nagative">2.8 <i class="fas fa-star"></i></span>
											</h5>
										</a>
										<ul>
											<li>fast food, cafe, italian</li>
											<li>11.30am - 11.30pm (mon-sun)</li>
											<li>cost $25 for two</li>
										</ul>
									</div>
								</div>
							</div>
							<div class="col-lg-4 col-sm-6 popular grid-item wow fadeInUp">
								<div class="special-box p-0 slider-sec">
									<div class="special-img">
										<div class="slide-1">
											<div>
												<img src="{{ asset('frontend/assets/images/restaurant/dishes/9.jpg')}}"
													 class="img-fluid blur-up lazyload bg-img" alt="">
											</div>
											<div>
												<img src="{{ asset('frontend/assets/images/restaurant/dishes/10.jpg')}}"
													 class="img-fluid blur-up lazyload bg-img" alt="">
											</div>
										</div>
									</div>
									<div class="special-content restaurant-detail">
										<a href="restaurant-single-1.html">
											<h5>italian restro
												<span class="positive">4.5 <i class="fas fa-star"></i></span>
											</h5>
										</a>
										<ul>
											<li>fast food, cafe, italian</li>
											<li>11.30am - 11.30pm (mon-sun)</li>
											<li>cost $25 for two</li>
										</ul>
									</div>
								</div>
							</div>
							<div class="col-lg-4 col-sm-6 trend grid-item wow fadeInUp">
								<div class="special-box p-0 slider-sec">
									<div class="special-img">
										<div class="slide-1">
											<div>
												<img src="{{ asset('frontend/assets/images/restaurant/dishes/10.jpg')}}"
													 class="img-fluid blur-up lazyload bg-img" alt="">
											</div>
											<div>
												<img src="{{ asset('frontend/assets/images/restaurant/dishes/11.jpg')}}"
													 class="img-fluid blur-up lazyload bg-img" alt="">
											</div>
										</div>
									</div>
									<div class="special-content restaurant-detail">
										<a href="restaurant-single-1.html">
											<h5>italian restro
												<span class="positive">4.5 <i class="fas fa-star"></i></span>
											</h5>
										</a>
										<ul>
											<li>fast food, cafe, italian</li>
											<li>11.30am - 11.30pm (mon-sun)</li>
											<li>cost $25 for two</li>
										</ul>
									</div>
								</div>
							</div>
							<div class="col-lg-4 col-sm-6 popular grid-item wow fadeInUp">
								<div class="special-box p-0 slider-sec">
									<div class="special-img">
										<div class="slide-1">
											<div>
												<img src="{{ asset('frontend/assets/images/restaurant/dishes/11.jpg')}}"
													 class="img-fluid blur-up lazyload bg-img" alt="">
											</div>
											<div>
												<img src="{{ asset('frontend/assets/images/restaurant/dishes/12.jpg')}}"
													 class="img-fluid blur-up lazyload bg-img" alt="">
											</div>
										</div>
									</div>
									<div class="special-content restaurant-detail">
										<a href="restaurant-single-1.html">
											<h5>italian restro
												<span class="positive">4.5 <i class="fas fa-star"></i></span>
											</h5>
										</a>
										<ul>
											<li>fast food, cafe, italian</li>
											<li>11.30am - 11.30pm (mon-sun)</li>
											<li>cost $25 for two</li>
										</ul>
									</div>
									<div class="label-offer">certified</div>
								</div>
							</div>
							<div class="col-lg-4 col-sm-6 latest grid-item wow fadeInUp">
								<div class="special-box p-0 slider-sec">
									<div class="special-img">
										<div class="slide-1">
											<div>
												<img src="{{ asset('frontend/assets/images/restaurant/dishes/12.jpg')}}"
													 class="img-fluid blur-up lazyload bg-img" alt="">
											</div>
											<div>
												<img src="{{ asset('frontend/assets/images/restaurant/dishes/13.jpg')}}"
													 class="img-fluid blur-up lazyload bg-img" alt="">
											</div>
										</div>
									</div>
									<div class="special-content restaurant-detail">
										<a href="restaurant-single-1.html">
											<h5>italian restro
												<span class="nagative">3.2 <i class="fas fa-star"></i></span>
											</h5>
										</a>
										<ul>
											<li>fast food, cafe, italian</li>
											<li>11.30am - 11.30pm (mon-sun)</li>
											<li>cost $25 for two</li>
										</ul>
									</div>
								</div>
							</div>
							<div class="col-lg-4 col-sm-6 latest grid-item wow fadeInUp">
								<div class="special-box p-0 slider-sec">
									<div class="special-img">
										<div class="slide-1">
											<div>
												<img src="{{ asset('frontend/assets/images/restaurant/dishes/12.jpg')}}"
													 class="img-fluid blur-up lazyload bg-img" alt="">
											</div>
											<div>
												<img src="{{ asset('frontend/assets/images/restaurant/dishes/13.jpg')}}"
													 class="img-fluid blur-up lazyload bg-img" alt="">
											</div>
										</div>
									</div>
									<div class="special-content restaurant-detail">
										<a href="restaurant-single-1.html">
											<h5>italian restro
												<span class="nagative">3.2 <i class="fas fa-star"></i></span>
											</h5>
										</a>
										<ul>
											<li>fast food, cafe, italian</li>
											<li>11.30am - 11.30pm (mon-sun)</li>
											<li>cost $25 for two</li>
										</ul>
									</div>
								</div>
							</div>
							<div class="col-lg-4 col-sm-6 latest grid-item wow fadeInUp">
								<div class="special-box p-0 slider-sec">
									<div class="special-img">
										<div class="slide-1">
											<div>
												<img src="{{ asset('frontend/assets/images/restaurant/dishes/13.jpg')}}"
													 class="img-fluid blur-up lazyload bg-img" alt="">
											</div>
											<div>
												<img src="{{ asset('frontend/assets/images/restaurant/dishes/14.jpg')}}"
													 class="img-fluid blur-up lazyload bg-img" alt="">
											</div>
										</div>
									</div>
									<div class="special-content restaurant-detail">
										<a href="restaurant-single-1.html">
											<h5>italian restro
												<span class="nagative">2.8 <i class="fas fa-star"></i></span>
											</h5>
										</a>
										<ul>
											<li>fast food, cafe, italian</li>
											<li>11.30am - 11.30pm (mon-sun)</li>
											<li>cost $25 for two</li>
										</ul>
									</div>
								</div>
							</div>
							<div class="col-lg-4 col-sm-6 popular grid-item wow fadeInUp">
								<div class="special-box p-0 slider-sec">
									<div class="special-img">
										<div class="slide-1">
											<div>
												<img src="{{ asset('frontend/assets/images/restaurant/dishes/14.jpg')}}"
													 class="img-fluid blur-up lazyload bg-img" alt="">
											</div>
											<div>
												<img src="{{ asset('frontend/assets/images/restaurant/dishes/8.jpg')}}"
													 class="img-fluid blur-up lazyload bg-img" alt="">
											</div>
										</div>
									</div>
									<div class="special-content restaurant-detail">
										<a href="restaurant-single-1.html">
											<h5>italian restro
												<span class="positive">4.5 <i class="fas fa-star"></i></span>
											</h5>
										</a>
										<ul>
											<li>fast food, cafe, italian</li>
											<li>11.30am - 11.30pm (mon-sun)</li>
											<li>cost $25 for two</li>
										</ul>
									</div>
								</div>
							</div>
						</div>
					</div>
					<nav aria-label="Page navigation example" class="pagination-section mt-0">
						<ul class="pagination">
							<li class="page-item">
								<a class="page-link" href="javascript:void(0)" aria-label="Previous">
									<span aria-hidden="true">&laquo;</span>
									<span class="sr-only">Previous</span>
								</a>
							</li>
							<li class="page-item active"><a class="page-link" href="javascript:void(0)">1</a></li>
							<li class="page-item"><a class="page-link" href="javascript:void(0)">2</a></li>
							<li class="page-item"><a class="page-link" href="javascript:void(0)">3</a></li>
							<li class="page-item">
								<a class="page-link" href="restaurant-slider.html#" aria-label="Next">
									<span aria-hidden="true">&raquo;</span>
									<span class="sr-only">Next</span>
								</a>
							</li>
						</ul>
					</nav>
				</div>
			</div>
		</div>
	</section>
	<!-- section End -->

	<!-- footer start -->
	@include('frontend.layouts.footer')
	<!-- footer end -->


	<!-- tap to top -->
	<div class="tap-top">
		<div>
			<i class="fas fa-angle-up"></i>
		</div>
	</div>
	<!-- tap to top end -->


	<!-- setting start -->
	@include('frontend.layouts.setting')
	<!-- setting end -->

	<!-- latest jquery-->

@endsection
