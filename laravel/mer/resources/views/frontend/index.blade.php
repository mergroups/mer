@extends('frontend.layouts.app')

@section('title')
	<title>{{ config('app.name', 'Laravel') }}</title>
@endsection

@section('content')
	<!-- header start -->
	@include('frontend.layouts.header')
	<!--  header end -->

	<!-- section start -->
	<section class="order-food-section  pt-0">
		<img src="{{ asset('frontend/assets/images/restaurant/background/1.jpg')}}" class="bg-img img-fluid blur-up lazyload" alt="">
		<div class="order-food">
			<div class="container">
				<div class="row">
					<div class="col-lg-8 offset-lg-2">
						<div class="book-table section-b-space p-0 single-table">
							<h3>The food you love, delivered with care</h3>
							<div class="table-form">
								<form>
									<div class="row w-100">
										<div class="form-group col-md-4">
											<input type="text" placeholder="enter your location" class="form-control">
										</div>
										<div class="form-group col-md-5">
											<input type="text" placeholder="what are you craving?" class="form-control">
										</div>
										<div class="search col-md-3">
											<a href="#" class="btn btn-rounded color1">find food</a>
										</div>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- section end -->


	<!-- categories section start -->
	<div class="container">
		<section class="category-part radius-category small-section pb-0 ratio_square">
			<div class="row">
				<div class="col">
					<div class="slide-6 no-arrow">
						<div class="category-block">
							<a href="#" tabindex="0">
								<div class="category-image">
									<img src="{{ asset('frontend/assets/images/restaurant/dishes/15.jpg')}}"
										 class="img-fluid blur-up lazyload bg-img" alt="">
								</div>
							</a>
							<div class="category-details">
								<a href="#" tabindex="0">
									<h3>maxican</h3>
								</a>
								<h6>23 restaurant</h6>
							</div>
						</div>
						<div class="category-block">
							<a href="#" tabindex="0">
								<div class="category-image">
									<img src="{{ asset('frontend/assets/images/restaurant/dishes/2.jpg')}}"
										 class="img-fluid blur-up lazyload bg-img" alt="">
								</div>
							</a>
							<div class="category-details">
								<a href="#" tabindex="0">
									<h3>italian</h3>
								</a>
								<h6>23 restaurant</h6>
							</div>
						</div>
						<div class="category-block">
							<a href="#" tabindex="0">
								<div class="category-image">
									<img src="{{ asset('frontend/assets/images/restaurant/dishes/11.jpg')}}"
										 class="img-fluid blur-up lazyload bg-img" alt="">
								</div>
							</a>
							<div class="category-details">
								<a href="#" tabindex="0">
									<h3>bakery</h3>
								</a>
								<h6>23 restaurant</h6>
							</div>
						</div>
						<div class="category-block">
							<a href="#" tabindex="0">
								<div class="category-image">
									<img src="{{ asset('frontend/assets/images/restaurant/dishes/10.jpg')}}"
										 class="img-fluid blur-up lazyload bg-img" alt="">
								</div>
							</a>
							<div class="category-details">
								<a href="#" tabindex="0">
									<h3>deserts</h3>
								</a>
								<h6>23 restaurant</h6>
							</div>
						</div>
						<div class="category-block">
							<a href="#" tabindex="0">
								<div class="category-image">
									<img src="{{ asset('frontend/assets/images/restaurant/dishes/14.jpg')}}"
										 class="img-fluid blur-up lazyload bg-img" alt="">
								</div>
							</a>
							<div class="category-details">
								<a href="#" tabindex="0">
									<h3>non veg</h3>
								</a>
								<h6>23 restaurant</h6>
							</div>
						</div>
						<div class="category-block">
							<a href="#" tabindex="0">
								<div class="category-image">
									<img src="{{ asset('frontend/assets/images/restaurant/dishes/7.jpg')}}"
										 class="img-fluid blur-up lazyload bg-img" alt="">
								</div>
							</a>
							<div class="category-details">
								<a href="#" tabindex="0">
									<h3>chinese</h3>
								</a>
								<h6>23 restaurant</h6>
							</div>
						</div>
						<div class="category-block">
							<a href="#" tabindex="0">
								<div class="category-image">
									<img src="{{ asset('frontend/assets/images/restaurant/dishes/8.jpg')}}"
										 class="img-fluid blur-up lazyload bg-img" alt="">
								</div>
							</a>
							<div class="category-details">
								<a href="#" tabindex="0">
									<h3>healthy</h3>
								</a>
								<h6>23 restaurant</h6>
							</div>
						</div>
						<div class="category-block">
							<a href="#" tabindex="0">
								<div class="category-image">
									<img src="{{ asset('frontend/assets/images/restaurant/dishes/9.jpg')}}"
										 class="img-fluid blur-up lazyload bg-img" alt="">
								</div>
							</a>
							<div class="category-details">
								<a href="#" tabindex="0">
									<h3>burger</h3>
								</a>
								<h6>23 restaurant</h6>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
	</div>
	<!-- categories section end -->


	<!-- collection banner -->
	<section class="p-t-0 section-b-space ratio_40">
		<div class="container">
			<div class="row partition2">
				<div class="col-md-6">
					<a href="#">
						<div class="collection-banner p-left text-start">
							<div class="img-part">
								<img src="{{ asset('frontend/assets/images/restaurant/dishes/17.jpg')}}"
									 class="img-fluid blur-up lazyload bg-img" alt="">
							</div>
							<div class="contain-banner">
								<div>
									<h4>Buy 1 get 1 free</h4>
									<h2>all pizza</h2>
								</div>
							</div>
						</div>
					</a>
				</div>
				<div class="col-md-6">
					<a href="#">
						<div class="collection-banner p-right text-end">
							<div class="img-part">
								<img src="{{ asset('frontend/assets/images/restaurant/dishes/16.jpg')}}"
									 class="img-fluid blur-up lazyload bg-img" alt="">
							</div>
							<div class="contain-banner">
								<div>
									<h4>only $12</h4>
									<h2>tasty burger</h2>
								</div>
							</div>
						</div>
					</a>
				</div>
			</div>
		</div>
	</section>
	<!-- collection banner end -->


	<!-- how to start section start -->
	<section class="section-b-space process-steps parallax-img">
		<img src="{{ asset('frontend/assets/images/restaurant/bg-4.jpg')}}" class="img-fluid blur-up lazyload bg-img" alt="">
		<div class="parallax-effect">
			<div class="food-img food1">
				<img src="{{ asset('frontend/assets/images/restaurant/food/1.png')}}">
			</div>
			<div class="food-img food2">
				<img src="{{ asset('frontend/assets/images/restaurant/food/2.png')}}">
			</div>
			<div class="food-img food3">
				<img src="{{ asset('frontend/assets/images/restaurant/food/3.png')}}">
			</div>
			<div class="food-img food4">
				<img src="{{ asset('frontend/assets/images/restaurant/food/4.png')}}">
			</div>
		</div>
		<div class="container">
			<div class="title-1 detail-title">
				<h2 class="pt-0">super easy booking</h2>
				<p class="font-design">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Adipisci alias aperiam
					at, aut commodi corporis dolorum ducimus labore magnam mollitia natus porro possimus quae sit
					tenetur veniam veritatis voluptate voluptatem!</p>
			</div>
			<div class="step-bg invert-lines">
				<div class="row">
					<div class="col-md-3">
						<div class="step-box">
							<div>
								<img src="{{ asset('frontend/assets/images/icon/order-steps/1.png')}}" class="img-fluid blur-up lazyload"
									 alt="">
								<h4>Order Food threw website or app</h4>
							</div>
						</div>
					</div>
					<div class="col-md-3">
						<div class="step-box">
							<div>
								<img src="{{ asset('frontend/assets/images/icon/order-steps/2.png')}}" class="img-fluid blur-up lazyload"
									 alt="">
								<h4>User receives confirmation</h4>
							</div>
						</div>
					</div>
					<div class="col-md-3">
						<div class="step-box">
							<div>
								<img src="{{ asset('frontend/assets/images/icon/order-steps/3.png')}}" class="img-fluid blur-up lazyload"
									 alt="">
								<h4>order processing & food preparation</h4>
							</div>
						</div>
					</div>
					<div class="col-md-3">
						<div class="step-box">
							<div>
								<img src="{{ asset('frontend/assets/images/icon/order-steps/4.png')}}" class="img-fluid blur-up lazyload"
									 alt="">
								<h4>food is on its way to deliver</h4>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- how to start section end -->


	<!-- section start -->
	<section class="section-b-space">
		<div class="container">
			<div class="row">
				<div class="col-12">
					<div class="filter-panel filter-title-bar">
						<h4 class="">popular restaurant</h4>
						<div class="left-filter ms-auto">
							<div class="filters respon-filter-content d-block filter-button-group">
								<ul>
									<li class="active" data-filter="*">All</li>
									<li data-filter=".popular">popular</li>
									<li data-filter=".latest">latest</li>
									<li data-filter=".trend">trend</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-12 ratio3_2">
					<div class="product-wrapper-grid special-section grid-box">
						<div class="row content grid mb-down">
							<div class="col-xl-3 col-lg-4 col-sm-6 popular grid-item" data-class="popular">
								<div class="special-box p-0">
									<div class="special-img">
										<a href="#">
											<img src="{{ asset('frontend/assets/images/restaurant/dishes/7.jpg')}}"
												 class="img-fluid blur-up lazyload bg-img" alt="">
										</a>
									</div>
									<div class="special-content restaurant-detail">
										<a href="#">
											<h5>italian restro
												<span class="positive">4.5 <i class="fas fa-star"></i></span>
											</h5>
										</a>
										<ul>
											<li>fast food, cafe, italian</li>
											<li>11.30am - 11.30pm (mon-sun)</li>
											<li>cost $25 for two</li>
										</ul>
									</div>
									<div class="label-offer">Recommended</div>
								</div>
							</div>
							<div class="col-xl-3 col-lg-4 col-sm-6 latest grid-item" data-class="latest">
								<div class="special-box p-0">
									<div class="special-img">
										<a href="#">
											<img src="{{ asset('frontend/assets/images/restaurant/dishes/8.jpg')}}"
												 class="img-fluid blur-up lazyload bg-img" alt="">
										</a>
									</div>
									<div class="special-content restaurant-detail">
										<a href="#">
											<h5>italian restro
												<span class="nagative">2.8 <i class="fas fa-star"></i></span>
											</h5>
										</a>
										<ul>
											<li>fast food, cafe, italian</li>
											<li>11.30am - 11.30pm (mon-sun)</li>
											<li>cost $25 for two</li>
										</ul>
									</div>
								</div>
							</div>
							<div class="col-xl-3 col-lg-4 col-sm-6 popular grid-item" data-class="popular">
								<div class="special-box p-0">
									<div class="special-img">
										<a href="#">
											<img src="{{ asset('frontend/assets/images/restaurant/dishes/9.jpg')}}"
												 class="img-fluid blur-up lazyload bg-img" alt="">
										</a>
									</div>
									<div class="special-content restaurant-detail">
										<a href="#">
											<h5>italian restro
												<span class="positive">4.5 <i class="fas fa-star"></i></span>
											</h5>
										</a>
										<ul>
											<li>fast food, cafe, italian</li>
											<li>11.30am - 11.30pm (mon-sun)</li>
											<li>cost $25 for two</li>
										</ul>
									</div>
								</div>
							</div>
							<div class="col-xl-3 col-lg-4 col-sm-6 trend grid-item" data-class="trend">
								<div class="special-box p-0">
									<div class="special-img">
										<a href="#">
											<img src="{{ asset('frontend/assets/images/restaurant/dishes/10.jpg')}}"
												 class="img-fluid blur-up lazyload bg-img" alt="">
										</a>
									</div>
									<div class="special-content restaurant-detail">
										<a href="#">
											<h5>italian restro
												<span class="positive">4.5 <i class="fas fa-star"></i></span>
											</h5>
										</a>
										<ul>
											<li>fast food, cafe, italian</li>
											<li>11.30am - 11.30pm (mon-sun)</li>
											<li>cost $25 for two</li>
										</ul>
									</div>
								</div>
							</div>
							<div class="col-xl-3 col-lg-4 col-sm-6 popular grid-item" data-class="popular">
								<div class="special-box p-0">
									<div class="special-img">
										<a href="#">
											<img src="{{ asset('frontend/assets/images/restaurant/dishes/11.jpg')}}"
												 class="img-fluid blur-up lazyload bg-img" alt="">
										</a>
									</div>
									<div class="special-content restaurant-detail">
										<a href="#">
											<h5>italian restro
												<span class="positive">4.5 <i class="fas fa-star"></i></span>
											</h5>
										</a>
										<ul>
											<li>fast food, cafe, italian</li>
											<li>11.30am - 11.30pm (mon-sun)</li>
											<li>cost $25 for two</li>
										</ul>
									</div>
									<div class="label-offer">Recommended</div>
								</div>
							</div>
							<div class="col-xl-3 col-lg-4 col-sm-6 latest grid-item" data-class="latest">
								<div class="special-box p-0">
									<div class="special-img">
										<a href="#">
											<img src="{{ asset('frontend/assets/images/restaurant/dishes/12.jpg')}}"
												 class="img-fluid blur-up lazyload bg-img" alt="">
										</a>
									</div>
									<div class="special-content restaurant-detail">
										<a href="#">
											<h5>italian restro
												<span class="nagative">2.8 <i class="fas fa-star"></i></span>
											</h5>
										</a>
										<ul>
											<li>fast food, cafe, italian</li>
											<li>11.30am - 11.30pm (mon-sun)</li>
											<li>cost $25 for two</li>
										</ul>
									</div>
								</div>
							</div>
							<div class="col-xl-3 col-lg-4 col-sm-6 popular grid-item" data-class="popular">
								<div class="special-box p-0">
									<div class="special-img">
										<a href="#">
											<img src="{{ asset('frontend/assets/images/restaurant/dishes/13.jpg')}}"
												 class="img-fluid blur-up lazyload bg-img" alt="">
										</a>
									</div>
									<div class="special-content restaurant-detail">
										<a href="#">
											<h5>italian restro
												<span class="positive">4.5 <i class="fas fa-star"></i></span>
											</h5>
										</a>
										<ul>
											<li>fast food, cafe, italian</li>
											<li>11.30am - 11.30pm (mon-sun)</li>
											<li>cost $25 for two</li>
										</ul>
									</div>
								</div>
							</div>
							<div class="col-xl-3 col-lg-4 col-sm-6 trend grid-item" data-class="trend">
								<div class="special-box p-0">
									<div class="special-img">
										<a href="#">
											<img src="{{ asset('frontend/assets/images/restaurant/dishes/14.jpg')}}"
												 class="img-fluid blur-up lazyload bg-img" alt="">
										</a>
									</div>
									<div class="special-content restaurant-detail">
										<a href="#">
											<h5>italian restro
												<span class="positive">4.5 <i class="fas fa-star"></i></span>
											</h5>
										</a>
										<ul>
											<li>fast food, cafe, italian</li>
											<li>11.30am - 11.30pm (mon-sun)</li>
											<li>cost $25 for two</li>
										</ul>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- section End -->


	<!-- app section start-->
	<section class="app-section pt-0 app-right-sec">
		<div class="container-fluid p-0">
			<div class="row m-0">
				<div class="col-md-6 p-0">
					<div class="app-content">
						<div>
							<h2 class="title">The best Food app</h2>
							<p>Quisque sollicitudin feugiat risus, eu posuere ex euismod eu. Phasellus hendrerit, massa
								efficitur dapibus pulvinar, sapien eros sodales ante, euismod aliquet nulla metus a
								mauris.
							</p>
							<h3>dowload app now...</h3>
							<div class="app-buttons">
								<a href="#" class="btn btn-curve"><i
										class="fab fa-apple"></i> app store</a>
								<a href="#" class="btn btn-curve white-btn"><i
										class="fab fa-android"></i> play store</a>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-6 p-0 position-relative">
					<div class="app-image">
						<img src="{{ asset('frontend/assets/images/restaurant/app.jpg')}}" class="img-fluid blur-up lazyload bg-img" alt="">
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- app section end -->


	<!-- footer start -->
	@include('frontend.layouts.footer')
	<!-- footer end -->

	<!-- tap to top -->
	@include('frontend.layouts.top')
	<!-- tap to top end -->

	<!-- setting start -->
	@include('frontend.layouts.setting')
	<!-- setting end -->

@endsection
