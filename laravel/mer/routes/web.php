<?php

	use Illuminate\Support\Facades\Route;


	Route::group(['middleware' => 'locale'], function () {
		// frontend
		Route::get('change-language/{language}', [App\Http\Controllers\Frontend\HomeController::class, 'changeLanguage'])->name('user.change-language');
		// home page
		Route::get('/', [App\Http\Controllers\Frontend\HomeController::class, 'index'])->name('home.index');
		// category page
		Route::get('/category', [App\Http\Controllers\Frontend\CategoryController::class, 'index'])->name('category.index');
		// blogs page
		Route::get('/blogs', [App\Http\Controllers\Frontend\BlogsController::class, 'index'])->name('blogs.index');
//          Route::get('/blogs/{id_blog}', [App\Http\Controllers\Frontend\BlogsController::class,'show')->name('blogs.detail');


		// admin
		Route::get('/admin', function () {
			return view('backend.index');
		});

		// redirect to 404 if not found page
		Route::fallback(function () {
			return view("frontend.404.index");
		});


	});






