import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
// import './assets/css/vendor/bootstrap.min.css';
// import './assets/css/vendor/font-awesome.css';
// import './assets/css/vendor/slick.css';
// import './assets/css/vendor/slick-theme.css';
// import './assets/css/vendor/base.css';
// import './assets/css/plugins/plugins.css';
// import './assets/css/style.css';
// import "slick-carousel/slick/slick.css";
// import "slick-carousel/slick/slick-theme.css";
// import Slider from "react-slick";
import App from './App';

ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById('root')
);
