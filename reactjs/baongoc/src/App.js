import React from 'react';
import MySwitcher from './components/MySwitcher';
import Header from './components/Header';
import MobileMenu from './components/MobileMenu';
import Banner from './components/Banner';
import Tab from './components/Tab';
import PostGrid from './components/PostGrid';
import Video from './components/Video';
import Instagram from './components/Instagram';
import Footer from './components/Footer';
import PostListWrapper from './components/PostListWrapper';
import CategoriesList from './components/CategoriesList';
import { BaoNgocProvider } from './components/data/dataBaoNgoc';

class App extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			menuHomePage: [
				{
					id: '002',
					text: 'MER 4535'
				},
				{
					id: '001',
					text: 'Bao Ngoc'

				},
				// {
				//     id: '003',
				//     text: '093.878.8091 - 0938.146.905(Zalo,SMS)'
				//
				// }
			],
			logoBaoNgoc: [
				{
					id:'004',
					link: 'logobaongoc.jpg',
					alt: 'Logo Bao Ngoc'
				}
			],
			dataCopyright: [
				// {
				// 	id: '005',
				// 	text: 'Contact Us'
				// },
				// {
				// 	id: '006',
				// 	text: 'Terms of Use'
				// },
				// {
				// 	id: '007',
				// 	text: 'AdChoices'
				// },
				{
					id: '008',
					text: 'MER Store'
				},
				{
					id: '009',
					text: 'Bao Ngoc Store'
				},
			],
			banner_main: {
				id: '010',
				link: 'assets/images/post-images/lifestyle-post-01.jpg',
				category: "Category Name",
				title: "Banner Chinh ",
				name: 'banner chinh',
				alt: 'banner chinh'
			},
			banner_child_1: {
				id: '011',
				link: 'assets/images/post-images/lifestyle-post-03.jpg',
				category: "Category Name",
				title: "Banner ben phai ",
				name: 'Banner ben phai ',
				alt: 'Banner ben phai '
			},
			banner_child_2: {
				id: '011',
				link: 'assets/images/post-images/lifestyle-post-04.jpg',
				category: "Category Name",
				title: "Banner ben phai ",
				name: 'Banner ben phai ',
				alt: 'Banner ben phai '
			},
			banner_child_3: {
				id: '013',
				link: 'assets/images/post-images/lifestyle-post-05.jpg',
				category: "Category Name",
				title: "Banner ben phai ",
				name: 'Banner ben phai ',
				alt: 'Banner ben phai '
			},
			banner_child_4: {
				id: '014',
				link: 'assets/images/post-images/lifestyle-post-02.jpg',
				category: "Category Name",
				title: "Banner ben phai ",
				name: 'Banner ben phai ',
				alt: 'Banner ben phai '
			},
		};
	}

	render() {
		console.log(React.version);
		return (
			<BaoNgocProvider value={this.state}>
				<div className="main-wrapper">
				<div className="mouse-cursor cursor-outer"/>
				<div className="mouse-cursor cursor-inner"/>
				{/* Start Header */}
				<Header/>
				{/* Start Header */}

				{/* Start Mobile Menu Area  */}
				<MobileMenu/>
				{/* End Mobile Menu Area  */}

				{/* Start Banner Area */}
				<Banner/>
				{/* End Banner Area */}

				{/* Start Tab Area  */}
				<Tab/>
				{/* End Tab Area  */}

				{/* Start Post Grid Area  */}
				<PostGrid/>
				{/* End Post Grid Area  */}

				{/* Start Video Area  */}
				<Video/>
				{/* End Video Area  */}

				{/* Start Categories List  */}
				<CategoriesList/>
				{/* Start Categories List  */}

				{/* Start Post List Wrapper  */}
				<PostListWrapper/>
				{/* End Post List Wrapper  */}

				{/* Start Instagram Area  */}
				<Instagram/>
				{/* End Instagram Area  */}

				{/* Start Footer Area  */}
				<Footer/>
				{/* End Footer Area  */}

				<MySwitcher/>

				{/* Start Back To Top  */}
				<a id="backto-top"/>
				{/* End Back To Top  */}
			</div>
			</BaoNgocProvider>
		);
	}


}

export default App;
