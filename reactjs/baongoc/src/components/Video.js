import React from 'react';

class Video extends React.Component {
    constructor(props)
    {
        super(props);
        this.state =
        {
            title: 'Featured Video',

        };
    }

    render() {
        return (
            <div className="axil-video-post-area axil-section-gap bg-color-black">
                <div className="container">
                    <div className="row">
                        <div className="col-lg-12">
                            <div className="section-title">
                                <h2 className="title">{ this.state.title }</h2>
                            </div>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-xl-6 col-lg-6 col-md-12 col-md-6 col-12">
                            {/* Start Post List  */}
                            <div className="content-block post-default image-rounded mt--30">
                                <div className="post-thumbnail">
                                    <a >
                                        <img src="assets/images/post-images/post-dark-01.jpg" alt="Post Images" />
                                    </a>
                                    <a className="video-popup position-top-center" ><span className="play-icon" /></a>
                                </div>
                                <div className="post-content">
                                    <div className="post-cat">
                                        <div className="post-cat-list">
                                            <a className="hover-flip-item-wrapper" >
                                                <span className="hover-flip-item">
                                                  <span data-text="CAREERS">CAREERS</span>
                                                </span>
                                            </a>
                                        </div>
                                    </div>
                                    <h3 className="title">
                                        <a >Security isn’t just a technology problem
                                        it’s about design, too</a>
                                    </h3>
                                    <div className="post-meta-wrapper">
                                        <div className="post-meta">
                                            <div className="content">
                                                <h6 className="post-author-name">
                                                    <a className="hover-flip-item-wrapper" >
                                                        <span className="hover-flip-item">
                                                          <span data-text="Rafayel Hasan">Rafayel Hasan</span>
                                                        </span>
                                                    </a>
                                                </h6>
                                                <ul className="post-meta-list">
                                                    <li>Feb 17, 2019</li>
                                                    <li>3 min read</li>
                                                </ul>
                                            </div>
                                        </div>
                                        <ul className="social-share-transparent justify-content-end">
                                            <li><a ><i className="fab fa-facebook-f" /></a></li>
                                            <li><a ><i className="fab fa-instagram" /></a></li>
                                            <li><a ><i className="fab fa-twitter" /></a></li>
                                            <li><a ><i className="fas fa-link" /></a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            {/* End Post List  */}
                        </div>
                        <div className="col-xl-6 col-lg-6 col-md-12 col-md-6 col-12">
                            <div className="row">
                                {/* Start Post List  */}
                                <div className="col-lg-6 col-md-6 col-sm-6 col-12">
                                    <div className="content-block post-default image-rounded mt--30">
                                        <div className="post-thumbnail">
                                            <a>
                                                <img src="assets/images/post-images/post-dark-04.jpg" alt="Post Images" />
                                            </a>
                                            <a className="video-popup size-medium position-top-center" ><span className="play-icon" /></a>
                                        </div>
                                        <div className="post-content">
                                            <div className="post-cat">
                                                <div className="post-cat-list">
                                                    <a className="hover-flip-item-wrapper" >
                                                    <span className="hover-flip-item">
                                                      <span data-text="LIFESTYLE">LIFESTYLE</span>
                                                    </span>
                                                    </a>
                                                </div>
                                            </div>
                                            <h5 className="title"><a >Get Ready To Up Your Creative Game With The </a></h5>
                                        </div>
                                    </div>
                                </div>
                                {/* End Post List  */}
                                {/* Start Post List  */}
                                <div className="col-lg-6 col-md-6 col-sm-6 col-12">
                                    <div className="content-block post-default  image-rounded mt--30">
                                        <div className="post-thumbnail">
                                            <a >
                                                <img src="assets/images/post-images/post-dark-03.jpg" alt="Post Images" />
                                            </a>
                                            <a className="video-popup size-medium position-top-center" ><span className="play-icon" /></a>
                                        </div>
                                        <div className="post-content">
                                            <div className="post-cat">
                                                <div className="post-cat-list">
                                                    <a className="hover-flip-item-wrapper" >
                        <span className="hover-flip-item">
                          <span data-text="DESIGN">DESIGN</span>
                        </span>
                                                    </a>
                                                </div>
                                            </div>
                                            <h5 className="title"><a >Traditional design won’t save us in the COVID-19 era</a></h5>
                                        </div>
                                    </div>
                                </div>
                                {/* End Post List  */}
                                {/* Start Post List  */}
                                <div className="col-lg-6 col-md-6 col-sm-6 col-12">
                                    <div className="content-block post-default image-rounded mt--30">
                                        <div className="post-thumbnail">
                                            <a >
                                                <img src="assets/images/post-images/post-dark-04.jpg" alt="Post Images" />
                                            </a>
                                            <a className="video-popup size-medium position-top-center" ><span className="play-icon" /></a>
                                        </div>
                                        <div className="post-content">
                                            <div className="post-cat">
                                                <div className="post-cat-list">
                                                    <a className="hover-flip-item-wrapper" >
                        <span className="hover-flip-item">
                          <span data-text="LEADERSHIP">LEADERSHIP</span>
                        </span>
                                                    </a>
                                                </div>
                                            </div>
                                            <h5 className="title"><a >New: Freehand Templates, built for the whole team</a></h5>
                                        </div>
                                    </div>
                                </div>
                                {/* End Post List  */}
                                {/* Start Post List  */}
                                <div className="col-lg-6 col-md-6 col-sm-6 col-12">
                                    <div className="content-block post-default image-rounded mt--30">
                                        <div className="post-thumbnail">
                                            <a >
                                                <img src="assets/images/post-images/post-dark-05.jpg" alt="Post Images" />
                                            </a>
                                            <a className="video-popup size-medium position-top-center" ><span className="play-icon" /></a>
                                        </div>
                                        <div className="post-content">
                                            <div className="post-cat">
                                                <div className="post-cat-list">
                                                    <a className="hover-flip-item-wrapper" >
                        <span className="hover-flip-item">
                          <span data-text="PRODUCT UPDATES">PRODUCT UPDATES</span>
                        </span>
                                                    </a>
                                                </div>
                                            </div>
                                            <h5 className="title"><a >The 1 tool that helps remote teams
                                                collaborate better</a></h5>
                                        </div>
                                    </div>
                                </div>
                                {/* End Post List  */}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }


}

export default Video;
