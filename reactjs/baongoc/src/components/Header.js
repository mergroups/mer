import React from 'react';
import HeaderTop from "./Header/HeaderTop";
import HeaderBottom from "./Header/HeaderBottom";
class Header extends React.Component {
	render() {
		return (
			<header className="header axil-header header-style-4  header-light header-sticky ">
				<HeaderTop/>
				<HeaderBottom/>
			</header>
		);
	}
}

export default Header;
