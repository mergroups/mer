import React from 'react';
import dataBaoNgoc  from '../components/data/dataBaoNgoc';
class Banner extends React.Component {
	static contextType = dataBaoNgoc;
	state = {
		banner_main: {},
		banner_child_1: {},
		banner_child_2: {},
		banner_child_3: {},
		banner_child_4: {},
	};

	componentDidMount()
	{
		let dataBaoNgoc = this.context;
		this.setState({
			banner_main : dataBaoNgoc.banner_main,
			banner_child_1 : dataBaoNgoc.banner_child_1,
			banner_child_2 : dataBaoNgoc.banner_child_2,
			banner_child_3 : dataBaoNgoc.banner_child_3,
			banner_child_4 : dataBaoNgoc.banner_child_4,
		});
	}
	render() {
		return (
			<div>
				<h1 className="d-none">Home Life Style Blog</h1>
				<div className="slider-area bg-color-grey ptb--80">
					<div className="axil-slide slider-style-2 plr--135 plr_lg--30 plr_md--30 plr_sm--30">
						<div className="row row--10">
							<div className="col-lg-12 col-xl-6 col-md-12 col-12">
								{/* Start Post Grid  */}
								<div className="content-block post-grid post-grid-transparent post-overlay-bottom">
									<div className="post-thumbnail" >
										<a >
											<img src={this.state.banner_main.link} alt={this.state.banner_main.name} />
										</a>
									</div>
									<div className="post-grid-content">
										<div className="post-content">
											<div className="post-cat">
												<div className="post-cat-list">
													<a className="hover-flip-item-wrapper" >
													  <span className="hover-flip-item">
														<span data-text={this.state.banner_main.category} >{this.state.banner_main.category}</span>
													  </span>
													</a>
												</div>
											</div>
											<h3 className="title"><a >{this.state.banner_main.title}</a></h3>
										</div>
									</div>
								</div>
								{/* Start Post Grid  */}
							</div>
							<div className="col-lg-12 col-xl-6 col-md-12 col-12 mt_lg--20 mt_md--20 mt_sm--20">
								<div className="row row--10">
									<div className="col-lg-6 col-md-6 col-sm-6 col-12">
										{/* Start Post Grid  */}
										<div className="content-block post-grid post-grid-transparent post-grid-small post-overlay-bottom">
											<div className="post-thumbnail">
												<a >
													<img src={this.state.banner_child_1.link} alt={this.state.banner_child_1.name} />
												</a>
											</div>
											<div className="post-grid-content">
												<div className="post-content">
													<div className="post-cat">
														<div className="post-cat-list">
															<a className="hover-flip-item-wrapper" >
															  <span className="hover-flip-item">
																<span data-text={this.state.banner_child_1.category}>{this.state.banner_child_1.category}</span>
															  </span>
															</a>
														</div>
													</div>
													<h5 className="title"><a >{this.state.banner_child_1.title}</a></h5>
												</div>
											</div>
										</div>
										{/* Start Post Grid  */}
									</div>
									<div className="col-lg-6 col-md-6 col-sm-6 mt_mobile--20 col-12">
										{/* Start Post Grid  */}
										<div className="content-block post-grid post-grid-transparent post-grid-small post-overlay-bottom">
											<div className="post-thumbnail">
												<a >
													<img src={this.state.banner_child_2.link} alt={this.state.banner_child_2.name} />
												</a>
											</div>
											<div className="post-grid-content">
												<div className="post-content">
													<div className="post-cat">
														<div className="post-cat-list">
															<a className="hover-flip-item-wrapper" >
															  <span className="hover-flip-item">
																<span data-text={this.state.banner_child_2.category} >{this.state.banner_child_2.category}</span>
															  </span>
															</a>
														</div>
													</div>
													<h5 className="title"><a >{this.state.banner_child_2.title}</a></h5>
												</div>
											</div>
										</div>
										{/* Start Post Grid  */}
									</div>
									<div className="col-lg-6 col-md-6 col-sm-6 col-12 mt--20">
										{/* Start Post Grid  */}
										<div className="content-block post-grid post-grid-transparent post-grid-small post-overlay-bottom">
											<div className="post-thumbnail">
												<a >
													<img src={this.state.banner_child_3.link} alt={this.state.banner_child_3.name} />
												</a>
											</div>
											<div className="post-grid-content">
												<div className="post-content">
													<div className="post-cat">
														<div className="post-cat-list">
															<a className="hover-flip-item-wrapper" >
															  <span className="hover-flip-item">
																<span data-text={this.state.banner_child_3.category} >{this.state.banner_child_3.category}</span>
															  </span>
															</a>
														</div>
													</div>
													<h5 className="title"><a >{this.state.banner_child_3.title}</a></h5>
												</div>
											</div>
										</div>
										{/* Start Post Grid  */}
									</div>
									<div className="col-lg-6 col-md-6 col-sm-6 col-12 mt--20">
										{/* Start Post Grid  */}
										<div className="content-block post-grid post-grid-transparent post-grid-small post-overlay-bottom">
											<div className="post-thumbnail">
												<a >
													<img src={this.state.banner_child_4.link} alt={this.state.banner_child_4.name} />
												</a>
											</div>
											<div className="post-grid-content">
												<div className="post-content">
													<div className="post-cat">
														<div className="post-cat-list">
															<a className="hover-flip-item-wrapper" >
															  <span className="hover-flip-item">
																<span data-text={this.state.banner_child_4.category} >{this.state.banner_child_4.category}</span>
															  </span>
															</a>
														</div>
													</div>
													<h5 className="title"><a >{this.state.banner_child_4.title}</a></h5>
												</div>
											</div>
										</div>
										{/* Start Post Grid  */}
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		);

	}

}

export default Banner;
