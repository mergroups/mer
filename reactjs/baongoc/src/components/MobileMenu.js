import React from 'react';

function MobileMenu() {
    return (
        <div className="popup-mobilemenu-area">
            <div className="inner">
                <div className="mobile-menu-top">
                    <div className="logo">
                        <a >
                            <img className="dark-logo" src="assets/images/logo/logo-black.png" alt="Logo Images" />
                            <img className="light-logo" src="assets/images/logo/logo-white2.png" alt="Logo Images" />
                        </a>
                    </div>
                    <div className="mobile-close">
                        <div className="icon">
                            <i className="fal fa-times" />
                        </div>
                    </div>
                </div>
                <ul className="mainmenu">
                    <li className="menu-item-has-children"><a >Home</a>
                        <ul className="axil-submenu">
                            <li><a >Home Default</a></li>
                            <li><a >Home Creative Blog</a></li>
                            <li><a >Home Seo Blog</a></li>
                            <li><a >Home Tech Blog</a></li>
                            <li><a >Home Lifestyle Blog</a></li>
                        </ul>
                    </li>
                    <li className="menu-item-has-children"><a >Categories</a>
                        <ul className="axil-submenu">
                            <li><a >Accessibility</a></li>
                            <li><a >Android Dev</a></li>
                            <li><a >Accessibility</a></li>
                            <li><a >Android Dev</a></li>
                        </ul>
                    </li>
                    <li className="menu-item-has-children"><a >Post Format</a>
                        <ul className="axil-submenu">
                            <li><a >Post Format Standard</a></li>
                            <li><a >Post Format Video</a></li>
                            <li><a >Post Format Gallery</a></li>
                            <li><a >Post Format Text Only</a></li>
                            <li><a >Post Layout One</a></li>
                            <li><a >Post Layout Two</a></li>
                            <li><a >Post Layout Three</a></li>
                            <li><a >Post Layout Four</a></li>
                            <li><a >Post Layout Five</a></li>
                        </ul>
                    </li>
                    <li className="menu-item-has-children"><a >Pages</a>
                        <ul className="axil-submenu">
                            <li><a >Post List</a></li>
                            <li><a >Post Archive</a></li>
                            <li><a >Author Page</a></li>
                            <li><a >About Page</a></li>
                            <li><a >Maintenance</a></li>
                            <li><a >Contact Us</a></li>
                        </ul>
                    </li>
                    <li><a >404 Page</a></li>
                    <li><a >Contact Us</a></li>
                </ul>
                <div className="buy-now-btn">
                    <a >Buy Now <span className="badge">$15</span></a>
                </div>
            </div>
        </div>
    );
}
export default MobileMenu;