import React from 'react';

const dataBaoNgoc = React.createContext({});

export const BaoNgocProvider = dataBaoNgoc.Provider;
export const BaoNgocConsumer = dataBaoNgoc.Consumer;
export default dataBaoNgoc;
