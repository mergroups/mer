import React from 'react';
import Clock from 'react-live-clock';


class HeaderTop extends React.Component {
	render() {
		return (
			<div className="header-top">
				<div className="container">
					<div className="row align-items-center">
						<div className="col-lg-6 col-md-8 col-sm-12">
							<div
								className="header-top-bar d-flex flex-wrap align-items-center justify-content-center justify-content-md-start">
								<ul className="header-top-date liststyle d-flex flrx-wrap align-items-center mr--20">
									<li>
										<a>
											<Clock
												ticking={true}
												format={'dddd - DD , MMMM - MM , YYYY, H:mm:ss A'}
											/>
										</a>
									</li>
								</ul>
							</div>
						</div>
						<div className="col-lg-6 col-md-4 col-sm-12">
							<ul className="social-share-transparent md-size justify-content-center justify-content-md-end">
								<li><a><i className="fab fa-facebook-f"/></a></li>
								<li><a><i className="fab fa-instagram"/></a></li>
								<li><a><i className="fab fa-twitter"/></a></li>
								<li><a><i className="fab fa-linkedin-in"/></a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		);
	}

}

export default HeaderTop;
