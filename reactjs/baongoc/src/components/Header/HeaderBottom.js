import React from 'react';
import MainMenuNav from "./MainMenuNav";
import HamburgerMenu from "./HamburgerMenu";
import dataBaoNgoc  from '../data/dataBaoNgoc';

class HeaderBottom extends React.Component {
    static contextType = dataBaoNgoc;
	state = {
		logoBaoNgoc: [],
	};

	componentDidMount()
	{
		let dataBaoNgoc = this.context;
		this.setState({
			logoBaoNgoc : dataBaoNgoc.logoBaoNgoc,
		});
	}

	render() {
		const logoBaoNgocs = this.state.logoBaoNgoc.map(logoBaoNgoc => {
			return (
				<img className="dark-logo" src={logoBaoNgoc.link} key={logoBaoNgoc.id}
					 alt={logoBaoNgoc.alt} />
			)
		})

		return (
			<div className="header-bottom">
				<div className="container">
					<div className="row justify-content-between align-items-center">
						<div className="col-xl-8 col-lg-4 col-md-4 col-12">
							<div
								className="wrapper d-block d-sm-flex flex-wrap align-items-center justify-content-center justify-content-md-start">
								<div className="logo">
									<a>
										{logoBaoNgocs}
									</a>
								</div>
								<div className="mainmenu-wrapper d-none d-xl-block">
									<nav className="mainmenu-nav">
										{/* Start Mainmanu Nav */}
										<MainMenuNav/>
										{/* End Mainmanu Nav */}
									</nav>
								</div>
							</div>
						</div>
						<div className="col-xl-4 col-lg-8 col-md-8 col-12">
							<div
								className="header-search d-flex flex-wrap align-items-center justify-content-center justify-content-md-end">
								<form className="header-search-form">
									<div className="axil-search form-group">
										<button type="submit" className="search-button"><i
											className="fal fa-search"/></button>
										<input type="text" className="form-control" placeholder="Search"/>
									</div>
								</form>
								<ul className="metabar-block">
									<li className="icon"><a><i className="fas fa-bookmark"/></a></li>
									<li className="icon"><a><i className="fas fa-bell"/></a></li>
									<li><a><img src="assets/images/others/author.png" alt="Author Images"/></a></li>
								</ul>
								{/* Start Hamburger Menu  */}
								<HamburgerMenu/>
								{/* End Hamburger Menu  */}
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	}
}

export default HeaderBottom;
