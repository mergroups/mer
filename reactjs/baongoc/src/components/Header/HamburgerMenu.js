import React from 'react';

function HamburgerMenu() {
    return (
        <div className="hamburger-menu d-block d-xl-none">
            <div className="hamburger-inner">
                <div className="icon"><i className="fal fa-bars"/></div>
            </div>
        </div>
    );
}

export default HamburgerMenu;
