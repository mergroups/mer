import React from 'react';
import dataBaoNgoc  from '../data/dataBaoNgoc';

class MainMenuNav extends React.Component {
	static contextType = dataBaoNgoc;
	state = {
		menuHomePage: [],
	};

	componentDidMount()
	{
		let dataBaoNgoc = this.context;
		this.setState({
			menuHomePage : dataBaoNgoc.menuHomePage,
		});
	}

	render() {
		const menuHome2 = this.state.menuHomePage.map(menuHomePage => {
			return(
				<li key={menuHomePage.id} className="menu-item-has-children"><a>{menuHomePage.text}</a></li>
			)
		})
		return (
			<ul className="mainmenu">
				{menuHome2}
			</ul>
		);
	}


}

export default MainMenuNav;
