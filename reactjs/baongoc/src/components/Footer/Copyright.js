import React from 'react';
import Clock from 'react-live-clock';
import dataBaoNgoc  from '../data/dataBaoNgoc';

class Copyright extends React.Component {
	static contextType = dataBaoNgoc;
	state = {
		logoBaoNgoc: [],
		dataCopyright: [],
	};

	componentDidMount()
	{
		let dataBaoNgoc = this.context;
		this.setState({
			logoBaoNgoc : dataBaoNgoc.logoBaoNgoc,
			dataCopyright: dataBaoNgoc.dataCopyright,
		});
	}

	render() {
		const Copyrights = this.state.dataCopyright.map(dataCopyright => {
			return (
				<li key={dataCopyright.id}>
					<a className="hover-flip-item-wrapper">
						<span className="hover-flip-item">
						  <span data-text={dataCopyright.text} >{dataCopyright.text}</span>
						</span>
					</a>
				</li>
			)
		})

		const logoBaoNgocs = this.state.logoBaoNgoc.map(BaoNgoc => {
			return (
				<img className="dark-logo" src={BaoNgoc.link} key={BaoNgoc.id}
					 alt={BaoNgoc.alt} />
			)
		})

		return (
			<div className="copyright-area">
				<div className="container">
					<div className="row align-items-center">
						<div className="col-lg-9 col-md-12">
							<div className="copyright-left">
								<div className="logo">
									<a>
										{logoBaoNgocs}
									</a>
								</div>
								<ul className="mainmenu justify-content-start">
									{Copyrights}
								</ul>
							</div>
						</div>
						<div className="col-lg-3 col-md-12">
							<div className="copyright-right text-left text-lg-right mt_md--20 mt_sm--20">
								<p className="b3">All Rights Reserved © <Clock
																			ticking={true}
																			format={'YYYY, H:mm:ss A'}
																		/>
								</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	}


}

export default Copyright;
