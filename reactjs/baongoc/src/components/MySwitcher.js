import React from 'react';

function MySwitcher() {
    return (
        <div id="my_switcher" className="my_switcher">
            <ul>
                <li>
                    <a  data-theme="light" className="setColor light">
                        <span title="Light Mode">Light</span>
                    </a>
                </li>
                <li>
                    <a  data-theme="dark" className="setColor dark">
                        <span title="Dark Mode">Dark</span>
                    </a>
                </li>
            </ul>
        </div>
    );
}

export default MySwitcher;
